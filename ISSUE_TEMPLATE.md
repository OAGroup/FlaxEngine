<!-- 在提交您的文件之前，请先搜索现有问题以查找可能的重复项：
https://github.com/flaxengine/FlaxEngine/issues?q=is%3Aissue
-->

**Issue description:**
<!-- 发生了什么，以及预期的结果。-->


**Steps to reproduce:**
<!-- 输入最小的复制步骤（如果有）。-->


**Minimal reproduction project:**
<!-- 推荐使用，因为它可以大大加快调试速度。拖放zip归档文件以将其上传。-->


**Flax version:**
<!-- 指定版本号。 -->

