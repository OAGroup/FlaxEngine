# 译文

<img align="left" src="Development/Images/Logo.png" width="100px"/>
<h1>Flax引擎</h1>
<a href="https://marketplace.visualstudio.com/items?itemName=Flax.FlaxVS"><img src="https://img.shields.io/badge/vs-extension-green.svg"/></a>
<a href="https://flaxengine.com/discord"><img src="https://discordapp.com/api/guilds/437989205315158016/widget.png"/></a>

Flax Engine是用C ++和C#编写的高质量现代3D游戏引擎。
从精美的图形到功能强大的脚本-Flax可以为您的游戏提供一切。专为快速工作流程而设计，具有许多随时可用的功能，正等着您。 要了解更多信息，请访问[网站](https://flaxengine.com).

该存储库包含FLAX的完整源代码（不包括受NDA保护的平台支持）。欢迎任何人在基于Flax的游戏中贡献或使用修改后的源代码。

# 发展历程

* [主页](https://flaxengine.com)
* [开发博客](https://flaxengine.com/blog)
* [文献资料](https://docs.flaxengine.com)
* [论坛](https://forum.flaxengine.com)
* [路线图](https://trello.com/b/NQjLXRCP/flax-roadmap)

# 屏幕截图

![渲染](Development/Images/flax-pic-1.jpg "渲染图")
![性能](Development/Images/flax-pic-3.jpg "高性能")
![渲染](Development/Images/flax-pic-2.jpg "PBR渲染")

# 入门

请按照以下说明从源代码编译和运行引擎。

## Windows

* 安装Visual Studio 2015或更高版本
* 安装Windows 8.1 SDK或更高版本
* 为桌面安装Microsoft Visual C ++ 2015.3 v140工具集（x86，x64）
* 克隆仓库（使用LFS）
* 运行 **GenerateProjectFiles.bat**
* 打开`Flax.sln`，并将解决方案配置设置为 **Editor.Development** ，并将解决方案平台设置为 **Win64**
* 编译Flax项目（按F7键）
* 运行Flax（按F5键）

## Linux

* 安装Visual Studio Code
* 安装 Mono ([https://www.mono-project.com/download/stable](https://www.mono-project.com/download/stable))
* 安装包含LFS的Git 
* 安装所需的软件包: `sudo apt-get install nuget autoconf libtool libogg-dev automake build-essential gettext cmake python curl libtool-bin libx11-dev libpulse-dev libasound2-dev libjack-dev portaudio19-dev`
* 安装编译器 `sudo apt-get install clang-7 lldb-7 lld-7`
* 克隆仓库（使用LFS）
* 运行 `sudo bash GenerateProjectFiles.sh`
* 使用Visual Studio Code打开工作区
* 构建并运行

# 用于Visual Studio的FLAX插件

Flax Visual Studio扩展提供了更好的编程工作流程，C#脚本调试功能，并允许附加到正在运行的引擎实例上以调试C#源代码。可以在[此处](https://marketplace.visualstudio.com/items?itemName=Flax.FlaxVS)下载此扩展程序。

## 工作区目录

- **Binaries/** - 可执行文件
  - **Editor/** - Flax编辑器二进制文件
  - **Tools/** - 工具二进制文件
- **Cache/** - 引擎和工具使用的本地数据缓存文件夹
  - **Intermediate/** - 中间文件和用于引擎构建的缓存
    - ***ProjectName*/** - 每个项目的构建缓存数据
    - **Deps/** - Flax.Build依赖关系构建缓存
  - **Projects/** - 项目文件位置
- **Content/** - 引擎和编辑器使用的资产和二进制文件
- **Development/** - 引擎开发文件
  - **Scripts/** - 实用程序脚本
- **packages/** - Nuget包缓存位置
- **Source/** - 源代码位置
  - **Editor/** - FLAX编辑器源代码
  - **Engine/** - FLAX引擎源代码
  - **Platforms/** - 每个平台的源文件和依赖文件
    - **DotNet/** - C#依赖
    - **Editor/** - FLAX编辑器二进制文件
    - ***PlatformName*/** - 每个平台的文件
      - **Binaries/** - 每个平台的二进制文件
        - **Game/** - FLAX游戏二进制文件
        - **Mono/** - Mono运行时文件和数据
        - **ThirdParty/** - 预先建立第三方二进制文件
  - **Shaders/** - 着色器源代码
  - **ThirdParty/** - 第三方源代码
  - **Tools/** - 开发工具源代码

# 许可和贡献

使用Flax源代码严格受Flax Engine最终用户许可协议的约束。如果您不同意这些条款（不时修订），则不允许您访问或使用FLAX引擎。

我们欢迎通过GitHub上的请求请求为Flax Engine开发做出任何贡献。我们大部分的积极开发都在master分支中，因此我们更喜欢在那接受拉取请求（特别是对于新功能）。我们试图确保所有新代码都符合FLAX编码标准。所有捐款均受EULA条款的约束。

# 原文

<img align="left" src="Development/Images/Logo.png" width="100px"/>
<h1>Flax Engine</h1>
<a href="https://marketplace.visualstudio.com/items?itemName=Flax.FlaxVS"><img src="https://img.shields.io/badge/vs-extension-green.svg"/></a>
<a href="https://flaxengine.com/discord"><img src="https://discordapp.com/api/guilds/437989205315158016/widget.png"/></a>

Flax Engine is a high quality modern 3D game engine written in C++ and C#.
From stunning graphics to powerful scripts - Flax can give everything for your games. Designed for fast workflow with many ready to use features waiting for you right now. To learn more see the website ([www.flaxengine.com](https://flaxengine.com)).

This repository contains full source code of the Flax (excluding NDA-protected platforms support). Anyone is welcome to contribute or use the modified source in Flax-based games.

# Development

* [Homepage](https://flaxengine.com)
* [Dev Blog](https://flaxengine.com/blog)
* [Documentation](https://docs.flaxengine.com)
* [Forum](https://forum.flaxengine.com)
* [Roadmap](https://trello.com/b/NQjLXRCP/flax-roadmap)

# Screenshots

![rendering](Development/Images/flax-pic-1.jpg "Rendering")
![performance](Development/Images/flax-pic-3.jpg "High Performance")
![pbr-rendering](Development/Images/flax-pic-2.jpg "PBR Rendering")

# Getting started

Follow the instructions below to compile and run the engine from source.

## Windows

* Install Visual Studio 2015 or newer
* Install Windows 8.1 SDK or newer
* Install Microsoft Visual C++ 2015.3 v140 toolset for desktop (x86, x64)
* Clone repo (with LFS)
* Run **GenerateProjectFiles.bat**
* Open `Flax.sln` and set solution configuration to **Editor.Development** and solution platform to **Win64**
* Compile Flax project (hit F7 key)
* Run Flax (hit F5 key)

## Linux

* Install Visual Studio Code
* Install Mono ([https://www.mono-project.com/download/stable](https://www.mono-project.com/download/stable))
* Install Git with LFS
* Install requried packages: `sudo apt-get install nuget autoconf libtool libogg-dev automake build-essential gettext cmake python curl libtool-bin libx11-dev libpulse-dev libasound2-dev libjack-dev portaudio19-dev`
* Install compiler `sudo apt-get install clang-7 lldb-7 lld-7`
* Clone repo (with LFS)
* Run `sudo bash GenerateProjectFiles.sh`
* Open workspace with Visual Studio Code
* Build and run

# Flax plugin for Visual Studio

Flax Visual Studio extension provides better programming workflow, C# scripts debugging functionality and allows to attach to running engine instance to debug C# source. This extension is available to download [here](https://marketplace.visualstudio.com/items?itemName=Flax.FlaxVS).

## Workspace directory

- **Binaries/** - executable files
  - **Editor/** - Flax Editor binaries
  - **Tools/** - tools binaries
- **Cache/** - local data cache folder used by engine and tools
  - **Intermediate/** - intermediate files and cache for engine build
    - ***ProjectName*/** - per-project build cache data
    - **Deps/** - Flax.Build dependencies building cache
  - **Projects/** - project files location
- **Content/** - assets and binary files used by engine and editor
- **Development/** - engine development files
  - **Scripts/** - utility scripts
- **packages/** - Nuget packages cache location
- **Source/** - source code lcoation
  - **Editor/** - Flax Editor source code
  - **Engine/** - Flax Engine source code
  - **Platforms/** - per-platform sources and dependency files
    - **DotNet/** - C# dependencies
    - **Editor/** - Flax Editor binaries
    - ***PlatformName*/** - per-platform files
      - **Binaries/** - per-platform binaries
        - **Game/** - Flax Game binaries
        - **Mono/** - Mono runtime files and data
        - **ThirdParty/** - prebuild 3rd Party binaries
  - **Shaders/** - shaders source code
  - **ThirdParty/** - 3rd Party source code
  - **Tools/** - development tools source code

# Licensing and Contributions

Using Flax source code is strictly governed by the Flax Engine End User License Agreement. If you don't agree to those terms, as amended from time to time, you are not permitted to access or use Flax Engine.

We welcome any contributions to Flax Engine development through pull requests on GitHub. Most of our active development is in the master branch, so we prefer to take pull requests there (particularly for new features). We try to make sure that all new code adheres to the Flax coding standards. All contributions are governed by the terms of the EULA.
